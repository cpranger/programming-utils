#pragma once

// my STL wish list (many are upcoming in c++17, some are of my own design)

namespace std
{
	// illegal addition of std::sign to the STL (AFAIK, no accepted proposals include it)
	// adapted from https://stackoverflow.com/a/4609795
	template< class T >
	constexpr int sign( T i ) { return (i > 0) - (i < 0); };
	
	// illegal addition of std::mod to the STL (AFAIK, no accepted proposals include it)
	// after https://codereview.stackexchange.com/a/58309 (and link therein)
	template< class T >
	constexpr T mod( T i, T n ) // the _actual_ modulus (not %, the remainder)
		{ return (i % n + n) % n; }
	
	// illegal addition of std::ipow to the STL (AFAIK, no accepted proposals include it)
	template< class T, typename std::enable_if_t<std::is_arithmetic<std::decay_t<T>>::value>* = nullptr >
	constexpr T ipow( T base, unsigned int exp, unsigned int i = 0 )
		{ return ( i < exp ) ? ( base * ipow<T>( base, exp, i+1 ) ) : ( T(1) ); }
	
	// Instead of c++17 std::invoke. C++11-safe version.
	// from http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2014/n4169.html
	// via  https://stackoverflow.com/questions/38288042/c11-14-invoke-workaround (doesn't compile with crayCC)
	template<typename Functor, typename... Args>
	typename std::enable_if<
	    std::is_member_pointer<typename std::decay<Functor>::type>::value,
	    typename std::result_of<Functor&&(Args&&...)>::type
	>::type
	invoke(Functor&& f, Args&&... args)
		{ return std::mem_fn(f)(std::forward<Args>(args)...); }
   
	template<typename Functor, typename... Args>
	typename std::enable_if<
	    !std::is_member_pointer<typename std::decay<Functor>::type>::value,
	    typename std::result_of<Functor&&(Args&&...)>::type
	>::type
	invoke(Functor&& f, Args&&... args)
		{ return std::forward<Functor>(f)(std::forward<Args>(args)...); }
	
	
	// Instead of c++17 std::apply.
	// See https://en.cppreference.com/w/cpp/utility/apply
	namespace detail {
		template <class F, class Tuple, std::size_t... I>
		constexpr auto apply_impl( F&& f, Tuple&& t, std::index_sequence<I...> ) {
			return std::invoke(std::forward<F>(f), std::get<I>(std::forward<Tuple>(t))...);
			// Note: std::invoke is a C++17 feature
		}
	}
	
	template <class F, class Tuple>
	constexpr auto apply(F&& f, Tuple&& t) {
		return detail::apply_impl(
		    std::forward<F>(f),
		    std::forward<Tuple>(t),
    	    std::make_index_sequence<std::tuple_size<std::decay_t<Tuple>>::value>{}
		);
	}
	
	// available from C++20
	// after https://stackoverflow.com/a/15202612
	struct identity {
		template<typename U>
		constexpr auto operator()(U&& v) const noexcept
			-> decltype(std::forward<U>(v))
			{ return std::forward<U>(v); }
	};
	
	// Generic flatten of containters of containers.
	// From https://stackoverflow.com/a/6404886
	// And  https://stackoverflow.com/q/6404856
	template< class InputIt, class OutputIt >
	void flatten( InputIt start, InputIt end, OutputIt dest ) {
		while( start != end ) {
			dest = std::copy( start->begin(), start->end(), dest );
    		++start;
    	}
	}
	
	// ostream operators
	namespace detail {
		template< class IT >
		std::ostream& print( std::ostream& stream, const IT& begin, const IT& end ) {
			stream << "{  ";
			for( auto it = begin; it != end; it++ )
				{ auto it2 = it; stream << *it << ( ++it2 != end ? ", " : "" ); }
			stream << "  }";
			return stream;
		}
	}
	
	template< class T, size_t N >
	std::ostream& operator<<( std::ostream& stream, const std::array<T,N>& object )
		{ return std::detail::print( stream, object.begin(), object.end() ); }
	
	template< class T >
	std::ostream& operator<<( std::ostream& stream, const std::vector<T>& object )
		{ return std::detail::print( stream, object.begin(), object.end() ); }
	
	// template< class ... T >
	// std::ostream& operator<<( std::ostream& stream, const std::set<T...>& object )
	// 	{ return std::detail::print( stream, object.begin(), object.end() ); }
	
	template< class ... T >
	std::ostream& operator<<( std::ostream& stream, const std::unordered_set<T...>& object )
		{ return std::detail::print( stream, object.begin(), object.end() ); }
	
	// template< class ... T >
	// std::ostream& operator<<( std::ostream& stream, const std::map<T...>& object )
	// 	{ return std::detail::print( stream, object.begin(), object.end() ); }
	
	template< class ... T >
	std::ostream& operator<<( std::ostream& stream, const std::unordered_map<T...>& object )
		{ return std::detail::print( stream, object.begin(), object.end() ); }
	
	// template< class ... T >
	// std::ostream& operator<<( std::ostream& stream, const std::list<T...>& object )
	// 	{ return std::detail::print( stream, object.begin(), object.end() ); }
	
	template< class T >
	std::ostream& operator<<( std::ostream& stream, const std::initializer_list<T>& object )
		{ return std::detail::print( stream, object.begin(), object.end() ); }
	
	template< class T1, class T2 >
	std::ostream& operator<<( std::ostream& stream, const std::pair<T1,T2>& object ) {
		stream << object.first << " -> " << object.second;
		return stream;
	}
	
	
	// overload of std::to_string for std::bitset argument
	template< size_t N >
	std::string to_string( std::bitset<N> value )
	{
		std::string result = "";
		for( int d = 0; d < N; d++ )
			result.push_back( value[d] + '0' ); // [1]
		return result;
		// 1: convert simple numeric type to char and append to string (https://stackoverflow.com/a/2279401).
	}
	
	
	
	
} // end namespace std


// Implementation of:
// 		https://en.cppreference.com/w/cpp/types/void_t
// 		https://en.cppreference.com/w/cpp/experimental/nonesuch
//		https://en.cppreference.com/w/cpp/experimental/is_detected
//		(all c++17 or experimental at the moment)

namespace std
{
	template< class ... >
	using void_t = void;
	
	struct nonesuch {
		nonesuch() = delete;
		~nonesuch() = delete;
		nonesuch(nonesuch const&) = delete;
		void operator=(nonesuch const&) = delete;
	};
	
	namespace detail
	{
		template< class Default, class AlwaysVoid, template<class...> class Op, class... Args>
		struct detector {
			using value_t = std::false_type;
			using type = Default;
		};
		
		template <class Default, template<class...> class Op, class... Args>
		struct detector<Default, std::void_t<Op<Args...>>, Op, Args...> {
			// Note that std::void_t is a C++17 feature
			using value_t = std::true_type;
			using type = Op<Args...>;
		};
		
	} // namespace detail
	
	template <template<class...> class Op, class... Args>
	using is_detected = typename detail::detector<nonesuch, void, Op, Args...>::value_t;
	
	template <template<class...> class Op, class... Args>
	using detected_t = typename detail::detector<nonesuch, void, Op, Args...>::type;
	
	template <class Default, template<class...> class Op, class... Args>
	using detected_or = detail::detector<Default, void, Op, Args...>;
	
	template< template<class...> class Op, class... Args >
	constexpr bool is_detected_v = is_detected<Op, Args...>::value;
	
	template< class Default, template<class...> class Op, class... Args >
	using detected_or_t = typename detected_or<Default, Op, Args...>::type;
	
	template <class Expected, template<class...> class Op, class... Args>
	using is_detected_exact = std::is_same<Expected, detected_t<Op, Args...>>;
	
	template <class Expected, template<class...> class Op, class... Args>
	constexpr bool is_detected_exact_v = is_detected_exact<Expected, Op, Args...>::value;
	
	template <class To, template<class...> class Op, class... Args>
	using is_detected_convertible = std::is_convertible<detected_t<Op, Args...>, To>;
	
	template <class To, template<class...> class Op, class... Args>
	constexpr bool is_detected_convertible_v = is_detected_convertible<To, Op, Args...>::value;
	
} // end namespace std




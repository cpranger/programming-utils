#define __CLASS_NAME_MACRO() \
static std::string __class_name() { \
	/* a magical member function that can be added to any class. Gets the pretty-printed type of the class */ \
	static const std::string function_name = BOOST_CURRENT_FUNCTION; \
	std::smatch   pieces_match; \
	auto regex = std::regex( "^static std::.*?string(?:\\<.*\\>)?\\s(\\w+.+)::__class_name\\(\\.*?\\).*?(\\[.*\\])?$" ); \
	if( std::regex_search( function_name, pieces_match, regex ) ) { \
		/* if( pieces_match[2].str().length() != 0 ) // output additional template arg info if available */ \
		/*	return pieces_match[1].str() + "    " + pieces_match[2].str(); */ \
		return pieces_match[1].str(); \
	} \
	return function_name + "    (failed to deduce class name)"; \
}

#define __CP(s) std::pair<std::string,typename std::add_lvalue_reference_t<decltype(s)>>( #s, s )
#define Name(s) std::pair<std::string,typename std::add_lvalue_reference_t<decltype(s)>>( #s, s )

#define DEFINE_ACCESSOR(s) decltype(s)& get_ ## s() { return s; }

#define DEFINE_CONST_ACCESSOR(s) decltype(s) const& get_ ## s() const { return s; }

#define IF_VERBOSE() \
	if( CheckOption( "-verbose_constructors" ) )

#define __VERBOSE_CONSTRUCTORS( t, i ) \
struct __verbose_constructor_struct \
{ \
	__verbose_constructor_struct() \
		{ IF_VERBOSE() std::cout << i << #t << "( ... )" << std::endl; } \
  \
	__verbose_constructor_struct( const __verbose_constructor_struct& ) \
		{ IF_VERBOSE() std::cout << i << #t << "( const " << #t << "& )" << std::endl; } \
  \
	__verbose_constructor_struct( __verbose_constructor_struct&& ) \
		{ IF_VERBOSE() std::cout << i << #t << "( " << #t << "&& )" << std::endl; } \
  \
	__verbose_constructor_struct& operator=( __verbose_constructor_struct && other ) \
		{ IF_VERBOSE() std::cout << i << #t << "& operator=( " << #t << "&& )" << std::endl; return *this; } \
  \
	__verbose_constructor_struct& operator=( const __verbose_constructor_struct & other ) \
		{ IF_VERBOSE() std::cout << i << #t << "& operator=( const " << #t << "& )" << std::endl; return *this; } \
  \
	~__verbose_constructor_struct() \
		{ IF_VERBOSE() std::cout << i << "~" << #t << "()" << std::endl; } \
  \
} __verbose_constructor_member

#define __VERBOSE_COPY_CONSTRUCT( other ) \
__verbose_constructor_member( other.__verbose_constructor_member )

#define __VERBOSE_MOVE_CONSTRUCT( other ) \
__verbose_constructor_member( std::move( other.__verbose_constructor_member ) )

#define __VERBOSE_ASSIGN( other ) \
__verbose_constructor_member = other.__verbose_constructor_member;


#define CONDITIONAL__( args... ) typename std::enable_if_t< args >
#define CONDITIONAL_(  args... ) CONDITIONAL__( args )*
#define CONDITIONAL(   args... ) CONDITIONAL_(  args ) = nullptr

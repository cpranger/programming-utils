template<class T>
class polymorphic_object_wrapper {
	// Provides an object-like interface to polymorphic classes
	// that can only be created at runtime and stored in pointers.
	// Based on the class unique_petscptr proposed by Alexey.
	// copy-constructor is provided through (virtual) clone() and
	// destroy() methods.
	
public:
	explicit polymorphic_object_wrapper( T* object = nullptr )            : object(object) {}
	polymorphic_object_wrapper( const polymorphic_object_wrapper& other ) : object(other.object->clone()) {}
	polymorphic_object_wrapper( polymorphic_object_wrapper&& other )      : object(other.release()) {}
	~polymorphic_object_wrapper() { destroy(); }
	
public:
	polymorphic_object_wrapper& operator=( const polymorphic_object_wrapper& other ) {
		if( this != &other ) 
			reset(other.object->clone());
		return *this;
	}
	polymorphic_object_wrapper& operator=( polymorphic_object_wrapper&& other ) {
		if( this != &other )
			reset(other.release());
		return *this;
	}
	T* operator->()
		{ return object; }
	const T* operator->() const
		{ return object; }
	T* get() const
		{ return object; }
	T** address()
		{ return &object; }
	T* release() {
		T *result = object;
		object = nullptr;
		return result;
	}
	void reset( T* _object = nullptr ) {
		destroy();
		object = _object;
	}
	polymorphic_object_wrapper clone() const
		{ return polymorphic_object_wrapper( object->clone() ); }
	
private:
	void destroy()
		{ delete object; }
	
private:
	T* object;
	
};

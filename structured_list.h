template< size_t N, class Ix = long int > class reverse_indexer;

template< size_t N, class Ix = long int >
class indexer
	// Compile-time recursion makes these methods flexible w.r.t. number of dimensions,
	// yet have been verified to compile to the exact same machine instructions as simple
	// dimension-specific code (e.g.: I = i + Ni ( j + Nj k ) ) ) with the -O3 flag.
{
	
protected:
	std::array<Ix,N> d; // decomposition (sizes)		// const?
	std::array<Ix,N> b; // base (starts)				// const?
	
public:
	indexer() {};
	indexer( const std::array<Ix,N>& d,
	         const std::array<Ix,N>& b = array_ext::make_uniform_array<Ix,N>(0)
	) : d(d), b(b) {}
	
protected:
	template< size_t _N = 0 >
	constexpr void i_impl( Ix& I, std::array<Ix,N>& i ) const
	{ // constexpr + -O3 makes this is conditional compilation, not execution.
		if( _N == N-2 )
			i[N-2] = I % d[N-2];
		else if( _N == N-1 && N > 1 )
			i[N-1] = ( I - i[N-2] ) / d[N-2];
		else if( _N == N-1 && N == 1 )
			i[0] = I;
		else {
			i[_N] = I % d[_N];
			I = ( I - i[_N] ) / d[_N];
		} if( _N < N-1 )
			i_impl<_N+(_N<N+1?1:0)>( I, i );
	}
	
public:
	constexpr Ix O( Ix n ) const { // offset // write recursively! TODO!
		Ix result = 1;
		for( Ix i = 0; i < n; i++ )
			result *= d[i];
		return result;
	}
	template< size_t _N = 0 >
	constexpr Ix I( const std::array<Ix,N>& i ) const
	{
		if( _N < N-1 )
			return b[_N] + i[_N] + d[_N] * I<_N+(_N<N-1?1:0)>(i);
		else
			return b[_N] + i[_N];
	}
	constexpr std::array<Ix,N> i( Ix I ) const
	{
		std::array<Ix,N> i = {{}};
		i_impl( I, i );
		return i-b;
	}
	constexpr bool in_bounds( const std::array<Ix,N>& i ) const // write recursively! TODO!
	{
		bool result = 1;
		for( Ix j = 0; j < N; j++ )
			result = result && ( 0 <= i[j]+b[j] && i[j]+b[j] < d[j] );
		return result;
	}
};

template< size_t N, class Ix >
class reverse_indexer : public indexer<N,Ix>
	// Slower than indexer. Use only in regions that are not performance-critical.
{
public:
	reverse_indexer( const std::array<Ix,N>& _d,
	                 const std::array<Ix,N>& _b = array_ext::make_uniform_array<Ix,N>(0)
	) : indexer<N,Ix>( _d, _b )
	{
		std::reverse(std::begin(this->d), std::end(this->d));
		std::reverse(std::begin(this->b), std::end(this->b));
	}

public:
	// indexing (reverse from indexer<N>, complies with standard c++ array indexing)
	Ix I( std::array<Ix,N> i ) const {
		if( !in_bounds(i) )
			std::cout << "reverse_indexer: Warning: index out of bounds!" << std::endl;
		
		std::reverse(std::begin(i), std::end(i));
		return indexer<N,Ix>::I(i);
	}
	
	std::array<Ix,N> i( Ix I ) const {
		auto i = indexer<N,Ix>::i(I);
		std::reverse(std::begin(i), std::end(i));
		return i;
	}
	
	bool in_bounds( std::array<Ix,N> i ) const {
		std::reverse(std::begin(i), std::end(i));
		return indexer<N,Ix>::in_bounds(i);
	}
};


template< class T, size_t N, class I = size_t >
class indexed_array
	: public std::shared_ptr<T>
{
	template<class,size_t,class>
	friend class indexed_array;
	
private:
	std::array<I,N> sizes;
	
private:
	// indexing (reverse from indexer<N>, complies with standard c++ array indexing)
	I index( std::array<I,N> i ) const {
		// check if i < sizes!!! TODO!
		// Mathematica implementation:
		// index[sizes_List][i_List] := FoldList[Times, 1, Most@Reverse@sizes].(Reverse@i)
		std::array<I,N> s = sizes;
		std::reverse(std::begin(i), std::end(i));
		std::reverse(std::begin(s), std::end(s));
		return array_ext::dot( constexpr_alg::fold_list( std::multiplies<I>(), (I) 1, array_ext::most(s) ), i );
	}
	
	std::array<I,N> indices( I index ) const {
		std::array<I,N> s = sizes;
		std::reverse(std::begin(s), std::end(s));
		auto i = indexer<N,I>(s).i( index );
		std::reverse(std::begin(i), std::end(i));
		return i;
	}
	
public:
	indexed_array( std::array<I,N> sizes )
		: std::shared_ptr<T>( new T[array_ext::product(sizes)], []( T* p ) { delete[] p; } )
		, sizes(sizes) {}
	
	// Hard copy. Copy all elements in data too.
	indexed_array( const indexed_array<T,N,I>& other ) // Does not preserve polymorphism!!!
	// Use T::clone instead if available? Check!! TODO!!
		: std::shared_ptr<T>( new T[other.size()], []( T* p ) { delete[] p; } )
		, sizes( other.sizes )
		{ loop( [this,&other]( std::array<I,N> i ) { (*this)[i] = T(other[i]); } ); }
	
	// Soft copy. Pass on data, only reset pointer according to index.
	indexed_array( const indexed_array<T,N+1,I>& other, I i ) // DO BOUNDS CHECKING!!! TODO!!
		: std::shared_ptr<T>( other, other.get()+other.index( std::array<I,N+1>{{i}} ) )
		, sizes(array_ext::most(other.sizes)) {}
	
	T& operator[]( std::array<I,N> i )
		{ return *(this->get()+index(i)); }
	
	const T& operator[]( std::array<I,N> i ) const
		{ return *(this->get()+index(i)); }
	
	template< class U = T, typename = typename std::enable_if<(N==1ul),U>::type >
	T& operator[]( I i ) // DO BOUNDS CHECKING!!! TODO!!
		{ return *(this->get()+i); }
	
	template< class U = T, typename = typename std::enable_if<(N==1ul),U>::type >
	const T& operator[]( I i ) const // DO BOUNDS CHECKING!!! TODO!!
		{ return *(this->get()+i); }
	
	template< class U = T, typename = typename std::enable_if<(N>1ul),U>::type >
	indexed_array<T,N-1,I> operator[]( I i )
		{ return indexed_array<T,N-1,I>( *this, i ); }
	
	template< class U = T, typename = typename std::enable_if<(N>1ul),U>::type >
	const indexed_array<T,N-1,I> operator[]( I i ) const
		{ return indexed_array<T,N-1,I>( *this, i ); }
	
	I size() const
		{ return array_ext::product(sizes); }
	
	template< class F >
	void loop( F&& f ) {
		for( I i = 0; i < size(); i++ )
			f(indices(i));
	}
	
	template< class F >
	void loop( F&& f ) const {
		for( I i = 0; i < size(); i++ )
			f(indices(i));
	}
	
};

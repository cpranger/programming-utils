
namespace type_utils  //  dealing with pointers
{
	template< class P >
	class is_plain_ptr {
		// in part after https://stackoverflow.com/a/10668045
		// in part after https://en.cppreference.com/w/cpp/language/sfinae
		typedef struct { char array[1]; } yes;
		typedef struct { char array[2]; } no;
		
		template< typename C > static yes& is_plain_ptr_(C*);
		template< typename C > static no & is_plain_ptr_(C);
		
	public:
		static bool const value = sizeof(is_plain_ptr_<std::decay_t<P>>(0)) == sizeof(yes);
	};
	
	template< class P, class T >
	class is_plain_ptr_of_type {
		// in part after https://stackoverflow.com/a/10668045
		// in part after https://en.cppreference.com/w/cpp/language/sfinae
		typedef struct { char array[1]; } yes;
		typedef struct { char array[2]; } no;
		
		template< typename C > static yes& is_plain_ptr_(C*);
		template< typename C > static no & is_plain_ptr_(C);
		
	public:
		static bool const value = sizeof(is_plain_ptr_<std::decay_t<P>>(0)) == sizeof(yes)
		                           &&   ( std::is_same<std::decay_t<P>,T*>::value
		                           ||     std::is_same<std::decay_t<P>,const T*>::value );
	};
	
	template< class P >
	class is_smart_ptr {
		// in part after https://stackoverflow.com/a/10668045
		// in part after https://en.cppreference.com/w/cpp/language/sfinae
		typedef struct { char array[1]; } yes;
		typedef struct { char array[2]; } no;
		
		template< typename C > static yes& has_deref(decltype(&C::operator*));
		template< typename C > static no & has_deref(...);
		template< typename C > static yes& has_arrow(decltype(&C::operator->));
		template< typename C > static no & has_arrow(...);
		
	public:
		static bool const value = sizeof(has_deref<std::decay_t<P>>(0)) == sizeof(yes)
		                       && sizeof(has_arrow<std::decay_t<P>>(0)) == sizeof(yes);
	};
	
	template< class P, class T >
	class is_smart_ptr_of_type {
		// in part after https://stackoverflow.com/a/10668045
		// in part after https://en.cppreference.com/w/cpp/language/sfinae
		typedef struct { char array[1]; } yes;
		typedef struct { char array[2]; } no;
		
		template< typename C > static yes& has_deref(decltype(&C::operator*));
		template< typename C > static no & has_deref(...);
		
		// and this is after https://stackoverflow.com/a/87846
		template< typename C, typename D, D* (C::*)() const> struct SFINAE {};
		template< typename C, typename D > static yes& has_arrow( SFINAE< C, D, &C::operator-> >* );
		template< typename C, typename D > static no & has_arrow(...);
		
	public:
		static bool const value = sizeof(has_deref<std::decay_t<P>>(0))   == sizeof(yes)
		                       && sizeof(has_arrow<std::decay_t<P>,T>(0)) == sizeof(yes);
	};
	
	template< class P >
	struct is_general_ptr
		{ static bool const value = is_plain_ptr<P>::value || is_smart_ptr<P>::value; };
	
	template< class P, class T >
	struct is_general_ptr_of_type
		{ static bool const value = is_plain_ptr_of_type<P,T>::value || is_smart_ptr_of_type<P,T>::value; };
	
}


namespace type_utils  //  dealing with closures
{
	// inspired by https://stackoverflow.com/a/7943765
	
	namespace detail
	{
		template< class F >
		using closure_t = decltype( &std::decay_t<F>::operator() );
		
		template< class T >
		struct closure_traits_impl : public closure_traits_impl< closure_t<T> > {};
		
		template < class T, class R, class ... A >
		struct closure_traits_impl< R (T::*)( A... ) const >
		{
			const static int arity = sizeof...(A);
			using return_t = R;
			
			template< size_t I >
			using args_t = typename std::tuple_element<I,std::tuple<A...>>::type;
		};
	}
		
	template< class T, class = std::void_t<> >
	struct closure_traits {}; // could have default members

	template< class T >
	struct closure_traits< T, std::void_t< detail::closure_t<T> > >
		: detail::closure_traits_impl<T> {};
	
	template< class F >
	using is_closure = std::is_detected<detail::closure_t,F>;
	
	template< class F >
	constexpr bool is_closure_v = is_closure<F>::value;
	
	template< class T, size_t N >
	constexpr bool closure_taking_n_plus_args_v = type_utils::closure_traits<T>::arity > N;
	
	// template< class F, size_t N >
	// constexpr bool is_closure_with_arity_v = is_closure_v<F> && type_utils::closure_traits<std::decay_t<F>>::arity == N;
	
	template< class T, size_t N >
	using closure_taking_n_plus_args = std::integral_constant<bool,closure_taking_n_plus_args_v<T>>;
	
	template< class T, size_t I >
	using closure_arg_i = typename closure_traits<T>::template args_t<I>;

	template< class T, size_t I, class A >
	using closure_arg_i_equals = std::is_same< std::decay_t< closure_arg_i<T,I> >, A >;
	
	template< class F, class T >
	struct is_closure_with_signature;
	
	template< class F, class R, class ... A >
	struct is_closure_with_signature<F,R(A...)>
		: std::is_convertible<std::decay_t<F>,std::function<R(A...)>> {};
	
	template< class F, class T >
	constexpr bool is_closure_with_signature_v = is_closure_with_signature<F,T>::value;
	
	namespace detail
	{
		template< class T, size_t I, class A >
		struct is_closure_taking_arg_i_equal_to_impl_3
		     : closure_arg_i_equals<T,I,A> {};
		
		
		template< class T, size_t I, class A, class Enable = void >
		struct is_closure_taking_arg_i_equal_to_impl_2 {};
	 	
		template< class T, size_t I, class A >
		struct is_closure_taking_arg_i_equal_to_impl_2< T, I, A, typename std::enable_if<  closure_taking_n_plus_args_v<T,I> >::type >
		     : is_closure_taking_arg_i_equal_to_impl_3< T, I, A > {};
		
		template< class T, size_t I, class A >
		struct is_closure_taking_arg_i_equal_to_impl_2< T, I, A, typename std::enable_if< !closure_taking_n_plus_args_v<T,I> >::type >
		     : std::false_type {};
		
		
		template< class T, size_t I, class A, class Enable = void >
		struct is_closure_taking_arg_i_equal_to_impl_1 {};
		
		template< class T, size_t I, class A >
		struct is_closure_taking_arg_i_equal_to_impl_1< T, I, A, typename std::enable_if<  is_closure_v<T> >::type >
		     : is_closure_taking_arg_i_equal_to_impl_2< T, I, A > {};
		
		template< class T, size_t I, class A >
		struct is_closure_taking_arg_i_equal_to_impl_1< T, I, A, typename std::enable_if< !is_closure_v<T> >::type >
		     : std::false_type {};
	}
	
	template< class T, size_t I, class A >
	using is_closure_taking_arg_i_equal_to = detail::is_closure_taking_arg_i_equal_to_impl_1< T, I, A >;

	template< class T, size_t I, class A >
	constexpr bool is_closure_taking_arg_i_equal_to_v = is_closure_taking_arg_i_equal_to< T, I, A >::value;
}

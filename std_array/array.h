// Copyright (C) 2007-2018 Free Software Foundation, Inc.
//
// This file is part of the GNU ISO C++ Library.  This library is free
// software; you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the
// Free Software Foundation; either version 3, or (at your option)
// any later version.

// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// Under Section 7 of GPL version 3, you are granted additional
// permissions described in the GCC Runtime Library Exception, version
// 3.1, as published by the Free Software Foundation.

// You should have received a copy of the GNU General Public License and
// a copy of the GCC Runtime Library Exception along with this program;
// see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see
// <http://www.gnu.org/licenses/>.

/*
	Compiled and Adapted from GCC8.2 libstdc++-v3 (see copyright notice above
	and licence files included in this directory). This version was cloned
	from commit 04535be00ef3c4f284623c61630c304ea2ba6e8f (refer to
	https://github.com/gcc-mirror/gcc). The entirety of file
	libstdc++-v3/include/std/array is included here, as well as parts of
	the following files:
	- libstdc++-v3/include/std/type_traits (for c++17 std::is_swappable and
	  std::is_nothrow_swappable)
	The main goal is to provide a C++17-compatible std::array (e.g., also
	including constexpr ___ operator[]( ___ ) const) to a non-C++17-compatible
	compiler (i.e.: without relying on C++17 syntax or extended C++17 library
	features).
	
	Adaptations are listed below:
	- _GLIBCXX17_CONSTEXPR is replaced by constexpr in order to emulate
	  C++17 constexpr rules.
	- _GLIBCXX_STD_C is replaced by std (which it anyway reduces to outside
	  of profile or debug mode).
	- _GLIBCXX17_INLINE is replaced by inline (as if using C++17)
	- __N( ... ) is removed: it seems to be primarily inteded as a note-to-self
	  for GCC developers.
	- std::__throw_out_of_range_fmt(<format>, <data>...) replaced by a proper
	  "throw std::out_of_range( <string literal> )"
	- boilerplate code that is included from libstdc++-v3/include/std/type_traits
	  is prepended by __xx. This is to avoid conflict if <type_traits> is
	  included under any compiler whose STL implementation is derived from GCC's.
	- __bool_constant<__> (defined in type_traits) is replaced by
	  std::integral_constant<bool,__> for aforementioned compatibility reasons.
	- all content placed in "inline namespace __1" for clang compatibility. Does
	  this break GCC or other compatibility? TEST, TODO!
	- overloads for tuple_element and tuple_size are split into separate implemen-
	  tations in clang and gcc, since in these compilers they are implemented as
	  class and struct, respectively. Any other compilers to pay attention to?
	- binary operator==and operator!= are disabled, since constexpr versions are
	  available in array_extension.h
	
	All in all, GCC implementation-specific code is either moved into this file
	(when it is needed and not available under C++14), or is deferred to the STL
	implementation used by the compiler that is including this file. 
	
*/

#pragma once

#ifdef  _GLIBCXX_ARRAY // GCC
	#error It seems that <array> is already included.
#endif
#define _GLIBCXX_ARRAY 1

#ifdef  _LIBCPP_ARRAY  // CLANG
	#error It seems that <array> is already included.
#endif
#define _LIBCPP_ARRAY  1

#ifdef  _ARRAY_        // MSVC
	#error It seems that <array> is already included.
#endif
#define _ARRAY_        1

#include <cstddef>
#include <utility>
#include <stdexcept>
#include <algorithm>
#include <iterator>
#include <string>
#include <type_traits>

// from libstdc++-v3/include/std/type_traits
namespace std { inline namespace __1
{
  namespace __xx__swappable_details {
    using std::swap;

    struct __xx__do_is_swappable_impl
    {
      template<typename _Tp, typename
               = decltype(swap(std::declval<_Tp&>(), std::declval<_Tp&>()))>
        static true_type __test(int);

      template<typename>
        static false_type __test(...);
    };

    struct __xx__do_is_nothrow_swappable_impl
    {
      template<typename _Tp>
        static std::integral_constant<bool,
          noexcept(swap(std::declval<_Tp&>(), std::declval<_Tp&>()))
        > __test(int);

      template<typename>
        static false_type __test(...);
    };

  } // namespace __swappable_details
  
  template<typename _Tp>
    struct __xx__is_swappable_impl
    : public __xx__swappable_details::__xx__do_is_swappable_impl
    {
      typedef decltype(__test<_Tp>(0)) type;
    };

  template<typename _Tp>
    struct __xx__is_nothrow_swappable_impl
    : public __xx__swappable_details::__xx__do_is_nothrow_swappable_impl
    {
      typedef decltype(__test<_Tp>(0)) type;
    };

  template<typename _Tp>
    struct __xx__is_swappable
    : public __xx__is_swappable_impl<_Tp>::type
    { };

  template<typename _Tp>
    struct __xx__is_nothrow_swappable
    : public __xx__is_nothrow_swappable_impl<_Tp>::type
    { };

/*// if these are needed, move to a separate header file and include that.
  template<typename _Tp>
    struct is_swappable
    : public __xx__is_swappable_impl<_Tp>::type
    { };

  /// is_nothrow_swappable
  template<typename _Tp>
    struct is_nothrow_swappable
    : public __xx__is_nothrow_swappable_impl<_Tp>::type
    { };

  /// is_swappable_v
  template<typename _Tp>
    inline constexpr bool is_swappable_v =
      is_swappable<_Tp>::value;

  /// is_nothrow_swappable_v
  template<typename _Tp>
    inline constexpr bool is_nothrow_swappable_v =
      is_nothrow_swappable<_Tp>::value;
*/
  
}} // inline namespace __1 // namespace std

// from libstdc++-v3/include/std/array
namespace std { inline namespace __1
{
  template<typename _Tp, std::size_t _Nm>
    struct __array_traits
    {
      typedef _Tp _Type[_Nm];
      typedef __xx__is_swappable<_Tp> _Is_swappable;
      typedef __xx__is_nothrow_swappable<_Tp> _Is_nothrow_swappable;

      static constexpr _Tp&
      _S_ref(const _Type& __t, std::size_t __n) noexcept
      { return const_cast<_Tp&>(__t[__n]); }

      static constexpr _Tp*
      _S_ptr(const _Type& __t) noexcept
      { return const_cast<_Tp*>(__t); }
    };

 template<typename _Tp>
   struct __array_traits<_Tp, 0>
   {
     struct _Type { };
     typedef true_type _Is_swappable;
     typedef true_type _Is_nothrow_swappable;

     static constexpr _Tp&
     _S_ref(const _Type&, std::size_t) noexcept
     { return *static_cast<_Tp*>(nullptr); }

     static constexpr _Tp*
     _S_ptr(const _Type&) noexcept
     { return nullptr; }
   };

  /**
   *  @brief A standard container for storing a fixed size sequence of elements.
   *
   *  @ingroup sequences
   *
   *  Meets the requirements of a <a href="tables.html#65">container</a>, a
   *  <a href="tables.html#66">reversible container</a>, and a
   *  <a href="tables.html#67">sequence</a>.
   *
   *  Sets support random access iterators.
   *
   *  @tparam  Tp  Type of element. Required to be a complete type.
   *  @tparam  N  Number of elements.
  */
  template<typename _Tp, std::size_t _Nm>
    struct array
    {
      typedef _Tp 	    			      value_type;
      typedef value_type*			      pointer;
      typedef const value_type*                       const_pointer;
      typedef value_type&                   	      reference;
      typedef const value_type&             	      const_reference;
      typedef value_type*          		      iterator;
      typedef const value_type*			      const_iterator;
      typedef std::size_t                    	      size_type;
      typedef std::ptrdiff_t                   	      difference_type;
      typedef std::reverse_iterator<iterator>	      reverse_iterator;
      typedef std::reverse_iterator<const_iterator>   const_reverse_iterator;

      // Support for zero-sized arrays mandatory.
      typedef std::__array_traits<_Tp, _Nm> _AT_Type;
      typename _AT_Type::_Type                         _M_elems;

      // No explicit construct/copy/destroy for aggregate type.

      // DR 776.
      void
      fill(const value_type& __u)
      { std::fill_n(begin(), size(), __u); }

      void
      swap(array& __other)
      noexcept(_AT_Type::_Is_nothrow_swappable::value)
      { std::swap_ranges(begin(), end(), __other.begin()); }

      // Iterators.
      constexpr iterator
      begin() noexcept
      { return iterator(data()); }

      constexpr const_iterator
      begin() const noexcept
      { return const_iterator(data()); }

      constexpr iterator
      end() noexcept
      { return iterator(data() + _Nm); }

      constexpr const_iterator
      end() const noexcept
      { return const_iterator(data() + _Nm); }

      constexpr reverse_iterator
      rbegin() noexcept
      { return reverse_iterator(end()); }

      constexpr const_reverse_iterator
      rbegin() const noexcept
      { return const_reverse_iterator(end()); }

      constexpr reverse_iterator
      rend() noexcept
      { return reverse_iterator(begin()); }

      constexpr const_reverse_iterator
      rend() const noexcept
      { return const_reverse_iterator(begin()); }

      constexpr const_iterator
      cbegin() const noexcept
      { return const_iterator(data()); }

      constexpr const_iterator
      cend() const noexcept
      { return const_iterator(data() + _Nm); }

      constexpr const_reverse_iterator
      crbegin() const noexcept
      { return const_reverse_iterator(end()); }

      constexpr const_reverse_iterator
      crend() const noexcept
      { return const_reverse_iterator(begin()); }

      // Capacity.
      constexpr size_type
      size() const noexcept { return _Nm; }

      constexpr size_type
      max_size() const noexcept { return _Nm; }

      constexpr bool
      empty() const noexcept { return size() == 0; }

      // Element access.
      constexpr reference
      operator[](size_type __n) noexcept
      { return _AT_Type::_S_ref(_M_elems, __n); }

      constexpr const_reference
      operator[](size_type __n) const noexcept
      { return _AT_Type::_S_ref(_M_elems, __n); }

      constexpr reference
      at(size_type __n)
      {
	if (__n >= _Nm)
	  // std::__throw_out_of_range_fmt("array::at: __n (which is %zu) "
	  // 					    ">= _Nm (which is %zu)",
	  // 					__n, _Nm);
	throw std::out_of_range( "array::at: __n (which is " + std::to_string(__n) + ") >= _Nm (which is " + std::to_string(_Nm) + ")" );
	return _AT_Type::_S_ref(_M_elems, __n);
      }

      constexpr const_reference
      at(size_type __n) const
      {
	// Result of conditional expression must be an lvalue so use
	// boolean ? lvalue : (throw-expr, lvalue)
	return __n < _Nm ? _AT_Type::_S_ref(_M_elems, __n)
	  : (throw std::out_of_range( "array::at: __n (which is " + std::to_string(__n) + ") >= _Nm (which is " + std::to_string(_Nm) + ")" ),
	     _AT_Type::_S_ref(_M_elems, 0));
      }

      constexpr reference
      front() noexcept
      { return *begin(); }

      constexpr const_reference
      front() const noexcept
      { return _AT_Type::_S_ref(_M_elems, 0); }

      constexpr reference
      back() noexcept
      { return _Nm ? *(end() - 1) : *end(); }

      constexpr const_reference
      back() const noexcept
      {
	return _Nm ? _AT_Type::_S_ref(_M_elems, _Nm - 1)
 	           : _AT_Type::_S_ref(_M_elems, 0);
      }

      constexpr pointer
      data() noexcept
      { return _AT_Type::_S_ptr(_M_elems); }

      constexpr const_pointer
      data() const noexcept
      { return _AT_Type::_S_ptr(_M_elems); }
    };
	
#if __cpp_deduction_guides >= 201606
  template<typename _Tp, typename... _Up>
    array(_Tp, _Up...)
      -> array<enable_if_t<(is_same_v<_Tp, _Up> && ...), _Tp>,
	       1 + sizeof...(_Up)>;
#endif

  // Array comparisons.
  /* DISABLED, constexpr version in array_extension.h
  template<typename _Tp, std::size_t _Nm>
    inline bool
    operator==(const std::__1::array<_Tp, _Nm>& __one, const std::__1::array<_Tp, _Nm>& __two)
    { return std::equal(__one.begin(), __one.end(), __two.begin()); }
  
  template<typename _Tp, std::size_t _Nm>
    inline bool
    operator!=(const std::__1::array<_Tp, _Nm>& __one, const std::__1::array<_Tp, _Nm>& __two)
    { return !(__one == __two); }
  */
		   
  template<typename _Tp, std::size_t _Nm>
    inline bool
    operator<(const std::__1::array<_Tp, _Nm>& __a, const std::__1::array<_Tp, _Nm>& __b)
    {
      return std::lexicographical_compare(__a.begin(), __a.end(),
					  __b.begin(), __b.end());
    }

  template<typename _Tp, std::size_t _Nm>
    inline bool
    operator>(const std::__1::array<_Tp, _Nm>& __one, const std::__1::array<_Tp, _Nm>& __two)
    { return __two < __one; }

  template<typename _Tp, std::size_t _Nm>
    inline bool
    operator<=(const std::__1::array<_Tp, _Nm>& __one, const std::__1::array<_Tp, _Nm>& __two)
    { return !(__one > __two); }

  template<typename _Tp, std::size_t _Nm>
    inline bool
    operator>=(const std::__1::array<_Tp, _Nm>& __one, const std::__1::array<_Tp, _Nm>& __two)
    { return !(__one < __two); }

  // Specialized algorithms.
  template<typename _Tp, std::size_t _Nm>
    inline
#if __cplusplus > 201402L || !defined(__STRICT_ANSI__) // c++1z or gnu++11
    // Constrained free swap overload, see p0185r1
    typename enable_if<
      std::__array_traits<_Tp, _Nm>::_Is_swappable::value
    >::type
#else
    void
#endif
    swap(std::__1::array<_Tp, _Nm>& __one, std::__1::array<_Tp, _Nm>& __two)
    noexcept(noexcept(__one.swap(__two)))
    { __one.swap(__two); }

#if __cplusplus > 201402L || !defined(__STRICT_ANSI__) // c++1z or gnu++11
  template<typename _Tp, std::size_t _Nm>
    typename enable_if<
      !std::__array_traits<_Tp, _Nm>::_Is_swappable::value>::type
    swap(std::__1::array<_Tp, _Nm>&, std::__1::array<_Tp, _Nm>&) = delete;
#endif

  template<std::size_t _Int, typename _Tp, std::size_t _Nm>
    constexpr _Tp&
    get(std::__1::array<_Tp, _Nm>& __arr) noexcept
    {
      static_assert(_Int < _Nm, "array index is within bounds");
      return std::__array_traits<_Tp, _Nm>::
	_S_ref(__arr._M_elems, _Int);
    }

  template<std::size_t _Int, typename _Tp, std::size_t _Nm>
    constexpr _Tp&&
    get(std::__1::array<_Tp, _Nm>&& __arr) noexcept
    {
      static_assert(_Int < _Nm, "array index is within bounds");
      return std::move(std::get<_Int>(__arr));
    }

  template<std::size_t _Int, typename _Tp, std::size_t _Nm>
    constexpr const _Tp&
    get(const std::__1::array<_Tp, _Nm>& __arr) noexcept
    {
      static_assert(_Int < _Nm, "array index is within bounds");
      return std::__array_traits<_Tp, _Nm>::
	_S_ref(__arr._M_elems, _Int);
    }

  template<std::size_t _Int, typename _Tp, std::size_t _Nm>
    constexpr const _Tp&&
    get(const std::__1::array<_Tp, _Nm>&& __arr) noexcept
    {
      static_assert(_Int < _Nm, "array index is within bounds");
      return std::move(std::get<_Int>(__arr));
    }
  
}} // inline namespace __1 // namespace std

namespace std
{
  #if defined(__clang__)
	
    inline namespace __1
	{
		/// tuple_size
		template<typename _Tp>
		class tuple_size;
		
		/// Partial specialization for std::array
		template<typename _Tp, std::size_t _Nm>
		class tuple_size<array<_Tp, _Nm>>
			: public integral_constant<std::size_t, _Nm> { };
		
		/// tuple_element
		template<std::size_t _Int, typename _Tp>
		class tuple_element;
		
		/// Partial specialization for std::array
		template<std::size_t _Int, typename _Tp, std::size_t _Nm>
		class std::tuple_element<_Int, array<_Tp, _Nm>>
		{
			static_assert(_Int < _Nm, "index is out of bounds");
			typedef _Tp type;
		};
	} // inline namespace __1
	
  #elif defined(__GNUC__) || defined(__GNUG__)
	
	/// tuple_size
	template<typename _Tp>
	struct tuple_size;
	
	/// Partial specialization for std::array
	template<typename _Tp, std::size_t _Nm>
	struct /*std::*/tuple_size<array<_Tp, _Nm>>
		: public integral_constant<std::size_t, _Nm> { };
	
	/// tuple_element
	template<std::size_t _Int, typename _Tp>
	struct tuple_element;
	
	/// Partial specialization for std::array
	template<std::size_t _Int, typename _Tp, std::size_t _Nm>
	struct /*std::*/tuple_element<_Int, array<_Tp, _Nm>>
	{
		static_assert(_Int < _Nm, "index is out of bounds");
		typedef _Tp type;
	};
	
	template<typename _Tp, std::size_t _Nm>
	struct __is_tuple_like_impl<array<_Tp, _Nm>> : true_type
		{ };
  #endif
	
} // namespace std


#include <array>

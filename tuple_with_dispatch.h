template< class T >
struct nonvoid;

template<>
struct nonvoid<void>{
	using type = bool;
	type operator()() { return true; }
};

template<class T>
struct nonvoid {
	using type = T;
	type operator()( type&& arg ) { return arg; }
};

template< class T >
using nonvoid_t = typename nonvoid<T>::type;


template< class ... O >
class tuple_with_dispatch : public std::tuple<O...>
{
public:
	template< class ... _O >
	tuple_with_dispatch( _O&& ... o )
		: std::tuple<O...>( O( std::forward<_O>(o) )... ) {}
	
	tuple_with_dispatch( const tuple_with_dispatch<O...>& other )
		: std::tuple<O...>( other ) {}
	
	tuple_with_dispatch( tuple_with_dispatch<O...>&& other )
		: std::tuple<O...>( other ) {}
	
public:
	using base_t = std::tuple<O...>;
	
	template< class F, class ... A >
	decltype(auto) apply( F&& f ) const
		{ return std::apply( std::forward<F>(f), static_cast<const std::tuple<O...>&>( *this ) ); }
	
	template< class F, class ... A >
	decltype(auto) apply( F&& f )
		{ return static_cast<const tuple_with_dispatch<O...>&>(*this).apply( std::forward<F>(f) ); }
	
	template< class F >
	struct compose_method_impl;
	
	template< class R, class ... A >
	struct compose_method_impl<R(A...)> {
		using type = std::tuple<R(std::remove_reference_t<O>::*)(A...)...>;
	};
	
	template< class R, class ... A >
	struct compose_method_impl<R(A...) const> {
		using type = std::tuple<R(std::remove_reference_t<O>::*)(A...) const...>;
	};
	
	template< class F >
	using compose_method = typename compose_method_impl<F>::type;
	
	// Maybe c++17 deduction guide?
	// template< class R, class ... A >
	// std::tuple<R(std::decay_t<O>::*)(A...)...>() -> compose_method<R(A...)>;
	
	template< class R, typename... A >
	decltype(auto) call_method( const std::tuple<R(std::decay_t<O>::*)(A...)...>& methods, A ... args ) {
		static_assert( std::tuple_size<std::decay_t<decltype(methods)>>::value > 0, "" );
		return call_method_impl(
			methods,
			std::make_index_sequence<sizeof...(O)>(),
			std::forward<A>(args)...
		);
	}
	
	template< class R, typename... A >
	decltype(auto) call_method( const std::tuple<R(std::decay_t<O>::*)(A...) const...>& methods, A ... args ) const {
		static_assert( std::tuple_size<std::decay_t<decltype(methods)>>::value > 0, "" );
		return call_method_impl(
			methods,
			std::make_index_sequence<sizeof...(O)>(),
			std::forward<A>(args)...
		);
	}
	
private:
	
	template< class R, class OB, class OP, class ... A, typename std::enable_if<!std::is_same<R,void>::value,int>::type = 0 >
	static R call_obj_method( OB&& obj, OP&& op, A&& ... args )
		{ return (obj.*op)( std::forward<A>(args)... ); }
	
	template< class R, class OB, class OP, class ... A, typename std::enable_if<std::is_same<R,void>::value,int>::type = 0 >
	static bool call_obj_method( OB&& obj, OP&& op, A&& ... args )
		{ (obj.*op)( std::forward<A>(args)... ); return true; }
	
	template< class R, size_t ... I, class ... A >
	decltype(auto) call_method_impl( const std::tuple<R(std::decay_t<O>::*)(A...)...>& methods, std::index_sequence<I...>, A&& ... args ) {
		return std::vector<nonvoid_t<R>>{
			call_obj_method<R>(
				std::get<I>(*this),
				std::get<I>(methods),
				std::forward<A>(args)...
			)...
		};
	}
	
	template< class R, size_t ... I, class ... A >
	decltype(auto) call_method_impl( const std::tuple<R(std::decay_t<O>::*)(A...) const...>& methods, std::index_sequence<I...>, A&& ... args ) const {
		return std::vector<nonvoid_t<R>>{
			call_obj_method<R>(
				std::get<I>(*this),
				std::get<I>(methods),
				std::forward<A>(args)...
			)...
		};
	}
};
